<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Rating</label>
    <protected>false</protected>
    <values>
        <field>NC__AccessControlSetting__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Controller__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__DescriptionLabel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Description__c</field>
        <value xsi:type="xsd:string">Take user input for ratings and review</value>
    </values>
    <values>
        <field>NC__DynamicPageURL__c</field>
        <value xsi:type="xsd:string">/predictivedonations</value>
    </values>
    <values>
        <field>NC__FriendlyURL2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__FriendlyURL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__HeaderImage__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__HeadingLabel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>NC__Url__c</field>
        <value xsi:type="xsd:string">/rating</value>
    </values>
</CustomMetadata>

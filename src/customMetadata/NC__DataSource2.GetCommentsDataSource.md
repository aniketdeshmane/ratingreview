<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GetCommentsDataSource</label>
    <protected>false</protected>
    <values>
        <field>NC__Class__c</field>
        <value xsi:type="xsd:string">NC.SOQLDataSource</value>
    </values>
    <values>
        <field>NC__Context__c</field>
        <value xsi:type="xsd:string">GetComments</value>
    </values>
    <values>
        <field>NC__Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__SObjectType__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>

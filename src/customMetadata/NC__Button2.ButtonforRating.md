<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ButtonforRating</label>
    <protected>false</protected>
    <values>
        <field>NC__AccessControlSetting__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Class__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Configuration__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__DataSource__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__IsPrimaryAction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>NC__Label__c</field>
        <value xsi:type="xsd:string">RateUs</value>
    </values>
    <values>
        <field>NC__Location__c</field>
        <value xsi:type="xsd:string">Right</value>
    </values>
    <values>
        <field>NC__OnClick__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__OnComplete__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__ReRender__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__SuccessLabel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Target__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__UrlParameterKey__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Url__c</field>
        <value xsi:type="xsd:string">/rating</value>
    </values>
</CustomMetadata>

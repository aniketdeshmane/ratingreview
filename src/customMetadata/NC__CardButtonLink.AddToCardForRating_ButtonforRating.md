<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AddToCardForRating_ButtonforRating</label>
    <protected>false</protected>
    <values>
        <field>NC__AccessControlSetting__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Button__c</field>
        <value xsi:type="xsd:string">ButtonforRating</value>
    </values>
    <values>
        <field>NC__Card__c</field>
        <value xsi:type="xsd:string">AddToCardForRating</value>
    </values>
    <values>
        <field>NC__IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>NC__SortOrder__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>NC__Tag__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>

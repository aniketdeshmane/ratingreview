<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TryForRating</label>
    <protected>false</protected>
    <values>
        <field>NC__CardComponentConstructor__c</field>
        <value xsi:type="xsd:string">FiveStarsConstructor</value>
    </values>
    <values>
        <field>NC__ClassName__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__ControllerName__c</field>
        <value xsi:type="xsd:string">FiveStarsController</value>
    </values>
    <values>
        <field>NC__Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Recommended2__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NC__Recommended__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>

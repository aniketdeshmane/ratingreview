<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Show Comments</label>
    <protected>false</protected>
    <values>
        <field>NC__AccessControlSetting__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__CSSClass__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__CardLayout__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Configuration__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__DataSource__c</field>
        <value xsi:type="xsd:string">GetCommentsDataSource</value>
    </values>
    <values>
        <field>NC__DescriptionLabel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__FieldSet__c</field>
        <value xsi:type="xsd:string">ShowRatingAndReviews</value>
    </values>
    <values>
        <field>NC__HeadingLabel__c</field>
        <value xsi:type="xsd:string">CommentsForThisProduct</value>
    </values>
    <values>
        <field>NC__NoRecordsLabel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__RecordTypeNames__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NC__Type__c</field>
        <value xsi:type="xsd:string">NC__RecordList</value>
    </values>
</CustomMetadata>

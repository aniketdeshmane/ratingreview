public class FiveStarsController extends NC.CardComponentController {
    public Integer rating { get; set; }
    public String review { get; set; }
    public void save() {
        RatingAndReview__c a = new RatingAndReview__c();
        String prodId;
        a.Review__c = review;
        a.RateNumber__c = rating;
        a.Product__c = ApexPages.currentPage().getParameters().get('Id');
        insert a;
        List<AggregateResult> avgrate = [
                Select Avg(RateNumber__c) avg from RatingAndReview__c where Product__r.ID = :a.Product__c
        ];
        Decimal val = (Decimal)avgrate[0].get('avg');
        if (val != 0) {
            List<NU__Product__c> products = [
                    Select AverageRating__c from NU__Product__c where Id = :a.Product__c
            ];
            products[0].AverageRating__c = val;
            update products[0];
        }

    }
    public PageReference submit() {
        // fetch the redirect from the url
        String next_url = ApexPages.currentPage().getParameters().get('Id');
        save();

        if (next_url != null && next_url.length() > 0) {
            PageReference next_page = new PageReference('/NC__Product?id=' + next_url);
            next_page.setRedirect(true);
            return next_page;
        } else {
            return(null);
        }
    }

}
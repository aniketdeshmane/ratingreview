public class FiveStarsConstructor implements NC.ICardComponentConstructor {
    public ApexPages.Component construct(NC.ICardComponentController controller) {
        return new Component.FiveStarsComponent(c = (FiveStarsController)controller);
    }
}